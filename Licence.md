# Terms and Conditions

The Quickbooks Extractor for KBC is built and offered by Kachna as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to extract the data from Quickbooks online to Keboola Connection Platform (KBC). 
API call is process by using user-entered keys for API authentication, no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## Quickbooks licence Terms
[Official website](https://community.intuit.com/questions/1379927)

## Contact

Kachna      
Vancouver, Canada (PST time)  
email: support@keboola.com    